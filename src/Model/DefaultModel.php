<?php

namespace App\Model;


use App\Dto\Request\AddDto;

/**
 * Class DefaultModel
 * @package App\Model
 */
class DefaultModel
{
    /**
     * @param AddDto $addDto
     * @return string
     */
    public static function addNameToList(AddDto $addDto)
    {
        return $addDto->name;
    }
}
